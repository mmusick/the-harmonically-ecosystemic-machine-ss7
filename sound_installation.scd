/**************************************************************

 Project: impact_installation_2015
    File: sound_installation.scd

  Author: Michael Musick
   Email: michael@michaelmusick.com

 Created: 2015-07-26 11:23:55
Modified: 2015-09-22 16:39:54


   Notes:


**************************************************************/

(
s.recChannels = 8;
s.recSampleFormat = 'int24';
s.record;
)

s.scope

Server.killAll;
(
o = Server.default.options;
o.memSize = 2**20;
o.numInputBusChannels  = 8;
o.numOutputBusChannels = 8;
o.hardwareBufferSize = 1024;
// o.device = "Avid 002 Rack";
// o.inDevice = "Built-in Microph";
// o.outDevice = "Built-in Output";
s.reboot;
);

(
s.makeWindow;
s.meter;
s.plotTree;
)


/**************************************************************************/
(
s.volume = -3.5;
Buffer.freeAll;
~path = Platform.userHomeDir ++ "/ss7/ss7/sythMusiCk/";
// ~path = Platform.userHomeDir ++ "/devel/SS7_cornerofalisteningroom/sythMusiCk/";

~restRatio = 0.95;
~grainWait = 4;
~noteWindowLength = 8;


~impact = ();
~impact.micChans = 6;
~impact.numMics = if(~impact.micChans.size>1, {~impact.micChans.size},{1});
// ~impact.speakerChan = [0,1,0,1,0];
~impact.speakerChan = [0,1,2,3,4,5,6,7];
~impact.numSpeakers = if(~impact.speakerChan.size>1, {~impact.speakerChan.size},{1});
~impact.outBus = Bus.audio(s, ~impact.numSpeakers);
~impact.restBus = Bus.control(s, 1);
~impact.grainBus = Bus.control(s, 1);

~impact.durList = [ 0.04166, 0.0625, 0.08333, 0.125, 0.16666, 0.25, 0.33333, 0.375, 0.41666, 0.5, 0.625, 0.66666, 0.75, 0.875, 0.91666, 1.0, 1.16666, 1.25, 1.33333, 1.375, 1.5, 1.66666, 1.75, 2.0, 2.16666, 2.25, 2.33333, 2.5, 3.0, 3.16666, 3.5, 3.75, 4.0 ];

~melody = ();
~melody.key = 0;
~melody.notes = [0,0];
~melody.durs = [1,1];

// Python Addr
~oscAddr = NetAddr.new( "127.0.0.1", 9000);
// Start Python
("python" + ~path++"py/chordgenerator.py").runInTerminal;
CmdPeriod.doOnce({"killall -v SIGKILL Python".unixCmd;});


~addr = NetAddr.localAddr;

(~path++"toLoad/*.scd").loadPaths;


// ~impact.numMics.do({
// 	arg id;
// 	Synth(\reflectiveMomentsAnalysis, [
// 		\micBus, ~rm_info.listener[id].micSig.index,
// 		\ampBus, ~rm_info.listener[id].ampSig.index,
// 		\krBus, ~rm_info.listener[id].mfccSig.index,
// 		\fluxBus, ~rm_info.listener[id].fluxBus.index,
// 		\onsetThresh, 0.4,
// 		\input, ~impact.micChans[id],
// 		\id, id
// 	]);
// });


Pdefn(\out1, ~impact.outBus.index + ( 0 % ~impact.numSpeakers ));
Pdefn(\out2, ~impact.outBus.index + ( 1 % ~impact.numSpeakers ));
Pdefn(\out3, ~impact.outBus.index + ( 2 % ~impact.numSpeakers ));
Pdefn(\out4, ~impact.outBus.index + ( 3 % ~impact.numSpeakers ));
Pdefn(\outB, ~impact.outBus.index + ( 4 % ~impact.numSpeakers ));
Pdef(\voice1).quant_(8).align([4, 0, 0]);
Pdef(\voice2).quant_(8).align([4, 0, 0]);
Pdef(\voice3).quant_(8).align([4, 0, 0]);
Pdef(\voice4).quant_(8).align([4, 0, 0]);
Pdef(\voiceB).quant_(8).align([4, 0, 0]);
// comment out any voices to reduce number
Pdef(\voice1).play;
Pdef(\voice2).play;
Pdef(\voice3).play;
Pdef(\voice4).play;
Pdef(\voiceB).play;





~sysOut = SynthDef(\masterOut, {
	arg input = 0, output = 0;
	var sig;

	sig = InFeedback.ar( input, ~impact.numSpeakers);

	sig = HPF.ar(sig, 30);
	sig = LowShelf.ar(sig, 88, dbgain: -3);
	sig = Limiter.ar(sig, 0.9, 0.1);

	Out.ar( ~impact.speakerChan[0], sig[0] );
	Out.ar( ~impact.speakerChan[1], sig[2] );
	Out.ar( ~impact.speakerChan[2], sig[1] );
	Out.ar( ~impact.speakerChan[3], sig[3] );
	Out.ar( ~impact.speakerChan[4], sig[4] );
	Out.ar( ~impact.speakerChan[5], sig[5] );
	Out.ar( ~impact.speakerChan[6], sig[6] );
	Out.ar( ~impact.speakerChan[7], sig[7] );
}).play(s,
	args: [\input, ~impact.outBus],
	addAction: \addToTail
);





OSCdef.newMatching(\changeRatios,
	{
		var restRatio, grainWait;

		grainWait = 4.0.rand;
		// "grain wait: ".post; grainWait.postln;
		restRatio = (0.3.rand);
		// "rest ratio: ".post; restRatio.postln;

		~ratioSynth.set(\restRatio, restRatio, \grainWait, grainWait);
	},
	'/changeRatios'
);
~ratioTask = Task({
	inf.do({
		~impact.restBus.get({arg val; ~restRatio = val; });
		~impact.grainBus.get({arg val; ~grainWait = val; });
		0.1.wait;
	});
}).start;


~ratioSynth = SynthDef(\ratioSynth, {
	arg restRatio = 0.9, grainWait = 4, lagTime = 10, restOut = 0, grainOut = 1;
	var restSig, grainSig;

	restSig = restRatio;
	restSig = restSig.lag(lagTime);
	Out.kr(restOut, restSig);

	grainSig = grainWait;
	grainSig = grainSig.lag(lagTime);
	Out.kr(grainOut, grainSig);

}).play( args: [\restOut, ~impact.restBus, \grainOut, ~impact.grainBus]);

SynthDef(\recordOutput, {
	Out.ar(5, SoundIn.ar(0));
	// Out.ar(6, SoundIn.ar(1));
}).play;



)