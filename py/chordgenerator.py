""" Functions etc. to generate chords using FSTs """

import json
import OSC
import types

# gross hack... probably a better way to do this
import os
import sys
# currdir = os.getcwd()
currdir = '/Users/Musick/devel/SS7/sythMusiCk/py/'
sys.path.append('/Users/Musick/devel')

currdir = '/Users/mmusick/devel/SS7_cornerofalisteningroom/sythMusiCk/py/'
sys.path.append('/Users/mmusick/devel')


print( 'current dir (currdir) = ' + currdir)
# p = os.path.join(currdir,'..','..')
# sys.path.append(p)
# p = '/Users/jon/Dropbox/work/src/'
# sys.path.append(p)

from hamilton.core import data as coredata
from hamilton.core import fsm
from hamilton.generate import chordgen
from hamilton.generate import chordsynth
from hamilton.core import utils as coreutils
import utils

# FST
FST_DIR = os.path.join(currdir,'..','fst')
LoG_FST_FILENAME = os.path.join(FST_DIR,'LoG.fst')              # maps melodic sequences to chords (roman numerals)
NGRAM_FILENAME = os.path.join(FST_DIR,'ngram.fst')              # n-gram chord model
RHYTHM_FST_FILENAME = os.path.join(FST_DIR,'rhythm.fst')        # rhythm model
PC_SYMS_FILENAME =os.path.join(FST_DIR,'input.syms')            # LoG input symbols (melody notes/pitch classes)
RN_SYMS_FILENAME = os.path.join(FST_DIR,'output.syms')          # LoG output symbols/R2C input symbols (roman numerals)
RHYTHM_SYM_FILENAME = os.path.join(FST_DIR,'rhythm.syms')       # rhythm symbols
RHYTHM_CODEBOOK_FILENAME = os.path.join(FST_DIR,'rhythm_codebook.json') # codebook to decode rhythm symbols

NUM_RHYTHMS_TO_GENERATE = 3

class Generator(object):
    """ Class to handle generation of chords from melodic sequences """

    NGRAM_ORDER = 5
    GEN_METHOD = chordgen.GEN_METHOD_LOG_PROB

    def __init__(self):
        """ Constructor """

        # load everything
        # symbol tables
        pc_syms_tab = fsm.SymbolTable(filename=PC_SYMS_FILENAME)
        pc_syms_tab.load_file()

        rn_syms_tab = fsm.SymbolTable(filename=RN_SYMS_FILENAME)
        rn_syms_tab.load_file()

        rhythm_syms_tab = fsm.SymbolTable(filename=RHYTHM_SYM_FILENAME)
        rhythm_syms_tab.load_file()

        # FSTs
        self.mel2roman = fsm.FST(filename=LoG_FST_FILENAME,\
            isyms_table=pc_syms_tab,\
            osyms_table=rn_syms_tab)
        self.ngram = fsm.FST(filename=NGRAM_FILENAME,\
            isyms_table=rn_syms_tab,\
            osyms_table=rn_syms_tab)
        self.rhythm_fst = fsm.FST(filename=RHYTHM_FST_FILENAME,\
            isyms_table=rhythm_syms_tab,\
            osyms_table=rhythm_syms_tab)

        # chord mapping structures
        self.chord_voicings = chordsynth.get_all_voicings()
        self.r2l_map = chordsynth.get_roman2chord_map()

        # rhythm symbol enconding/decoding
        # load the symbols->durations codebook (decoder)
        self.rhythm_sym2dur_codebook = None
        with open(RHYTHM_CODEBOOK_FILENAME,'r') as fp:
            self.rhythm_sym2dur_codebook = json.load(fp)

        # build the durations->symbols codebook (enconder)
        self.rhythm_dur2sym_codebook = {}
        for sym,dur in self.rhythm_sym2dur_codebook.items():
            self.rhythm_dur2sym_codebook[float(dur)] = sym

        self.melodic_context = []
        self.harmonic_context = []

    def rhythm_duration_to_sym(self,dur):
        """ Convert a rhythm symbol, e.g. 1.0, to a symbols (e.g. 'k2').
        Returns None if dur not found.
        """

        if dur in self.rhythm_dur2sym_codebook:
            return self.rhythm_dur2sym_codebook[dur]
        else:
            return None

    def rhythm_sym_to_duration(self,sym):
        """ Convert a rhythm symbol, e.g. 'k2', to a duration.
        Returns 0.0 if symbol not found.
        """

        if sym in self.rhythm_sym2dur_codebook:
            return self.rhythm_sym2dur_codebook[sym]
        else:
            return 0.0


    def generate_rhythm(self,input_rhythm_sequence):
        """ generate a rhythm sequence """

        # convert input durations to symbols
        in_duration_syms = [self.rhythm_duration_to_sym(d) for d in input_rhythm_sequence] # if not s == fsm.EPSILON_LABEL]

        # try getting an accomp. sequence from FST
        _,rhythm_seq = chordgen.get_rhythm_seq(in_duration_syms=in_duration_syms,\
            codebook=self.rhythm_sym2dur_codebook,\
            rhythm_fst=self.rhythm_fst)

        if rhythm_seq == []:
            rhythm_seq = chordgen.get_rand_gen_rhythm_seq(rhythm_fst=self.rhythm_fst,\
                codebook=self.rhythm_sym2dur_codebook,\
                pref_tot_dur=16.0)

        # rhythm_seq = chordgen.get_rhythm_seq(rhythm_fsa=self.rhythm_fsa,\
        #     codebook=self.rhythm_codebook)

        return rhythm_seq

    def generate_chord(self,melodic_sequence,start_times,end_times,key=0):
        """ Generate a chord given a list of pitch classes

        Parameters
        ----------
        out_roots: list of ints
            the pitch class of the root of the chord
        melodic_sequence: list of ints
            a list of integers representing pitch class of melody notes

        Returns
        -------
        out_voicings: list of lists of ints
            a list of the chord voicings for each chord (list of MIDI notes)
        chord_durations: list of floats
            list of durations for each chord
        """

        # current melodic sequence
        sc_mel_seq = [ coreutils.transpose_pc_to_c(n,key) for n in melodic_sequence ]
        sc_mel_seq = ['pc' + str(m) for m in sc_mel_seq]
        melnotes = self.melodic_context + sc_mel_seq

        mel_seq = coredata.Sequence(labels=melnotes,start_times=start_times,end_times=end_times)
        # generate new harmonic sequence from current harmonic sequence
        # plus melodic context, if any
        chord_seq = chordgen.get_harmonic_seq(melody_sequence=mel_seq,\
                full_fst=self.mel2roman,\
                melody_fst_filename='melfst.fst',\
                out_harmony_fst_filename='harmfst.fst',\
                method=self.GEN_METHOD,
                npaths=1)
        # mel_seq_eps,chord_seq = chordgen.get_harmonic_seq(melody_seq=melseq,\
        #     full_fst=self.mel2roman,\
        #     method=self.GEN_METHOD)

        # add in previous harmonic context, if any
        # chord_seq = self.harmonic_context + chord_seq

        # get last melody and harmony segments for context
        # idx = chordgen.segment_sequences_by_harmony(melodic_seq=mel_seq_eps,\
        #     harmonic_seq=chord_seq)
        # starti,endi = idx[-1]
        # self.melodic_context = mel_seq_eps[starti:endi+1]
        # self.harmonic_context = chord_seq[starti:endi+1]

        out_voicings = []
        chord_durations = []
        # out_roots = []
        prev_voicing = None

        # for chord in chord_seq.get_labels():
        for event in chord_seq.events:
            chord = event.label

            if chord == fsm.EPSILON_LABEL:
                continue

            if not chord in self.r2l_map:
                print 'ERROR: symbol ',chord,\
                    'not found in Roman numeral to chord label map'
                continue

            # print 'rn:',chord,
            print 'GENERATOR: roman=',chord,
            chord = self.r2l_map[chord]
            # print 'label:',chord

            chord = coreutils.transpose_chord_from_c(chord=chord,to_key=key)
            # root = coreutils.get_root_pc(chord)
            # out_roots.append(root)

            print 'chord:',chord   #,'root:',root
            voicing = chordsynth.choose_voicing(chord_name=chord,\
                voicings=self.chord_voicings,\
                prev_voicing=prev_voicing)
            prev_voicing = voicing

            # voicing.insert(0,root)
            out_voicings.append(voicing)
            dt = event.end_time - event.start_time
            chord_durations.append(dt)

        return out_voicings,chord_durations

def handle_timeout(self):
    """ this method of reporting timeouts only works by convention
    that before calling handle_request() field .timed_out is
    set to False
    """

    self.timed_out = True


class Communicator(object):
    """ Class to communicate with Supercollider via OSC """

    # OSC/Supercollider communication
    OSC_URL = '127.0.0.1'
    # OSC_PORT = 9000
    OSC_RECV_PORT = 9000
    OSC_SEND_PORT = 9001
    OSC_RECV_PATH = "/sc2py"
    OSC_SEND_PATH = "/py2sc"

    def __init__(self,generator):
        """ Constructor """

        self.generator = generator

        self.server_init()
        self.client_init()

    def server_init(self):
        """ Initialize server (to listen for OSC messages) """

        print 'Initializing server url:',self.OSC_URL,'port:',\
            self.OSC_RECV_PORT,'path:',self.OSC_RECV_PATH

        self.server = OSC.OSCServer((self.OSC_URL,self.OSC_RECV_PORT))
        self.server.timeout = 0
        self.server.handle_timeout = types.MethodType(handle_timeout,\
            self.server)
        self.server.addMsgHandler(self.OSC_RECV_PATH,\
            self.receive_msg )

    def client_init(self):
        """ Initialize client (to send OSC messages) """

        print 'Initializing client url:',self.OSC_URL,'port:',\
            self.OSC_SEND_PORT,'path:',self.OSC_SEND_PATH

        self.client = OSC.OSCClient()
        self.client.connect((self.OSC_URL,self.OSC_SEND_PORT))

    def each_frame(self):
        """ user script called every frame """
        # clear timed_out flag
        self.server.timed_out = False

        # handle all pending requests then return
        while not self.server.timed_out:
            self.server.handle_request()

    def run(self):
        """ Main run loop """
        try:
            while True:
                self.each_frame()
                # time.sleep(1)

        except KeyboardInterrupt:
            self.stop()

    def stop(self):
        print 'closing OSC client/server'
        self.server.close()
        print 'done'

    def receive_msg(self, path, tags, args, source):
        """ handle incoming messages from Supercollider """
        # print 'OSC message:',path,'\t',tags,'\t',args,'\t',source

        key,melody_notes,in_rhythms = utils.parse_msg(args[0])

        if melody_notes == []:
            return

        start_times,end_times = utils.rhythm_vector_to_times(durations=in_rhythms)
        # out_roots,out_notes = self.generator.generate_chord(melody_notes,key)
        out_notes,chord_durations = self.generator.generate_chord(melodic_sequence=melody_notes,\
            start_times=start_times,\
            end_times=end_times,
            key=key)
        # rhythm_seq = self.generator.generate_rhythm()
        # print 'RHYTHM:',str(rhythm_seq)

        # generate rhythms
        out_rhythms = []
        for i in range(NUM_RHYTHMS_TO_GENERATE):
            rhythm = self.generator.generate_rhythm(input_rhythm_sequence=in_rhythms[:-1])
            out_rhythms.append(rhythm)

        # format the messages
        chord_msg = []
        for notes in out_notes:
            curr_chord_msg = [str(n) for n in notes]
            curr_chord_msg = ','.join(curr_chord_msg)
            chord_msg.append(curr_chord_msg)           
        chord_msg = '^'.join(chord_msg)

        duration_msg = [str(d) for d in chord_durations]
        duration_msg = '^'.join(duration_msg)

        rhythm_msg = []
        for rhy in out_rhythms:
            curr_rhy_msg = [str(r) for r in rhy]
            curr_rhy_msg = ','.join(curr_rhy_msg)
            rhythm_msg.append(curr_rhy_msg)
        rhythm_msg = '^'.join(rhythm_msg)

        msg = chord_msg + '@' + duration_msg + '@' + rhythm_msg
        print 'OSC send msg:',msg

        self.send_msg(msg=str(msg))


    def send_msg(self,msg):
        """ Send message back to Supercollider """
        # bundle = OSC.OSCBundle()
        # bundle.append( {'addr':self.OSC_SEND_PATH, 'args':msg} )
        # self.client.send(bundle)
        osc_msg = OSC.OSCMessage(self.OSC_SEND_PATH)
        osc_msg.append(msg)
        self.client.send(osc_msg)

if __name__ == '__main__':
    generator = Generator()
    communicator = Communicator(generator=generator)
    communicator.run()


